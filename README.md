# Theo's Setup

## Development IDE

### Visual Studio Code

I have listed some of the main extensions I use in VSCode below:

- Prettier
- ESLint
- Error Lens
- Todo Tree
- Remote Development Extension Pack

### Neovim

[You can my lua configuration file here](dotfiles/.config/nvim/init.lua).

## Shell

<img alt="Terminal" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Breezeicons-apps-48-utilities-terminal.svg/1200px-Breezeicons-apps-48-utilities-terminal.svg.png" width="50px" height="50px">

I am using the zsh shell along with the powerlevel10k theme.
[Here is my zsh configuration file.](dotfiles/.zshrc)

![ZSH Example Screenshot](assets/zsh.png)

## Operating System

<img alt="Operating System" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Archlinux-icon-crystal-64.svg/1200px-Archlinux-icon-crystal-64.svg.png" width="50" height="50">

I am running Arch Linux, alongside Windows 11 Pro. I use Arch Linux as my daily driver. For gaming and VR, I use my windows 11 installation.

## Window Manager

My Arch Linux installation is also running the bspwm window manager, along with polybar for a status bar and sxhkd for keybindings. 

